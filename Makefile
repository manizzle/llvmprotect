.PHONY: all tests clean

all:
	./run.sh

tests: all
	./test.sh
clean:
	rm -rf pass/build/
