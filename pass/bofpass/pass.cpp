#include "llvm/Pass.h"
#include "llvm/IR/Function.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/IR/LegacyPassManager.h"
#include "llvm/Transforms/IPO/PassManagerBuilder.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/DataLayout.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/IR/Module.h"
#include "llvm/Transforms/Utils/BasicBlockUtils.h"
#include "llvm/IR/InstIterator.h"
#include "llvm/IR/Verifier.h"
#include <cstring>
#include <vector>

using namespace llvm;

namespace {
    class BOF : public FunctionPass {
    public:
        static char ID;
        BOF() : FunctionPass(ID) {}
        // For free, find the ret/invoke calls
        // to do the frees for the function as that when
        // alloca should cleanup
        // use invoke for exception handling/longjmp
        // use ret for other function cleanup
        
        // 1) insert malloc calls
        // 2) insert free calls
        
        bool runOnFunction(Function &F) override {
            Module *M = F.getParent();
            DataLayout DL = DataLayout(M);
            std::vector<Instruction*> deletion;
            std::vector<Instruction*> mallocs;

            // create types for malloc call
            Constant *cM = M->getOrInsertFunction("malloc", FunctionType::get(Type::getInt8PtrTy(F.getContext()),
                                                                              ArrayRef<Type*>(Type::getInt64Ty(F.getContext())), false));
            Function *malloc = cast<Function>(cM);
            
            uint64_t total = 0;
            uint64_t shift = 0;
            for (BasicBlock &BB: F) {
                for (Instruction &I: BB) {
                    if (AllocaInst *alloca = dyn_cast<AllocaInst>(&I)) {
                        if (alloca->getName().size()) {
                            total += DL.getTypeAllocSize(alloca->getAllocatedType());
                        }
                    }
                }
            }
            
            // Allocate 1 large buffer for all local variables
            ConstantInt *a = ConstantInt::get(Type::getInt64Ty(F.getContext()), total);
            IRBuilder<> builder(&*(&*F.begin())->begin());
            Instruction* CallMalloc = builder.CreateCall(malloc, ArrayRef<Value *>(a), "malloc");
            Value* MallocVal = CallMalloc;
            mallocs.push_back(CallMalloc);
            
            for (BasicBlock &BB: F) {
                for (Instruction &I: BB) {
                    if (AllocaInst *alloca = dyn_cast<AllocaInst>(&I)) {
                        if (alloca->getName().size()) {
                            // 1. List all allocated buffers
                            
                            //errs() << alloca->getOpcodeName() << " " << alloca->getName() << " " << *alloca->getAllocatedType() << " " << DL.getTypeAllocSize(alloca->getAllocatedType()) << "\n";
                            
                            // 2. Create 1 indexed memory object + bitcast + propogate to other functions
                            
                            IRBuilder<> builder(&I);
                            // create malloc index
                            std::vector<llvm::Value*> vect_1;
                            vect_1.push_back(builder.getInt32(shift));
                            llvm::GetElementPtrInst* gep1 = llvm::GetElementPtrInst::CreateInBounds(MallocVal, llvm::ArrayRef<llvm::Value*>(vect_1) , "retval", &I);
                            Value* CurrMallocVal = gep1;
                            // bitmask index to proper alloca type
                            Value *Bitcast = builder.CreateBitCast(MallocVal, alloca->getType(), "malloc_cast");
                            shift += DL.getTypeAllocSize(alloca->getAllocatedType());
                            I.replaceAllUsesWith(Bitcast);
                            // mark for deletion
                            deletion.push_back(&I);
                        }
                    }
                    // when alloca's are cleaned up, similarly, clean up this function
                    if (dyn_cast<ReturnInst>(&I) || dyn_cast<InvokeInst>(&I)) {
                        for (Instruction *MM : mallocs) {
                            Value *MallocVal = MM;
                            Instruction *CallFree = CallInst::CreateFree(MallocVal, &I);
                        }
                        mallocs.clear();
                    }
                }
            }
            // delete original alloca calls
            for (Instruction *U : deletion)
                U->eraseFromParent();

            // debug
            llvm::verifyModule(*F.getParent());
        }
    };
}

char BOF::ID = 0;

// Register Pass for Opt
static RegisterPass<BOF> X("bofpass", "Mitigate Buffer Overflows", false, false);

// Register Pass for Clang
static RegisterStandardPasses Z(PassManagerBuilder::EP_EarlyAsPossible,
                                [](const PassManagerBuilder &, legacy::PassManagerBase &PM) \
                                -> void { PM.add(new BOF()); });
